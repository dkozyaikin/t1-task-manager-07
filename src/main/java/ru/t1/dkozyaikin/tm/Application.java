package ru.t1.dkozyaikin.tm;

import ru.t1.dkozyaikin.tm.model.Command;
import ru.t1.dkozyaikin.tm.util.FormatUtil;

import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.*;
import static ru.t1.dkozyaikin.tm.constant.TerminalConst.*;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        System.out.println("Task Manager started");
        if (processArguments(args)) exit();
        processCommands();
    }

    public static void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nEnter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processCommand(final String parameter) {
        if (parameter == null || parameter.isEmpty()) {
            displayErrorCommand();
            return;
        }
        switch (parameter) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_INFO:
                displayInfo();
                break;
            case CMD_EXIT:
                exit();
            default: {
                displayErrorCommand();
            }
        }
    }

    public static boolean processArguments(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void processArgument(final String parameter) {
        if (parameter == null || parameter.isEmpty()) {
            displayErrorCommand();
            return;
        }
        switch (parameter) {
            case ARG_HELP:
                displayHelp();
                break;
            case ARG_VERSION:
                displayVersion();
                break;
            case ARG_ABOUT:
                displayAbout();
                break;
            case ARG_INFO:
                displayInfo();
                break;
            default: {
                displayErrorCommand();
            }
        }
    }

    private static void displayErrorCommand() {
        System.out.println("Error! This command not supported...");
    }

    private static void displayHelp() {
        System.out.println(Command.HELP);
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.EXIT);
    }

    private static void displayVersion() {
        System.out.println("1.5.0");
    }

    private static void displayAbout() {
        System.out.println("Denis Kozyaykin");
        System.out.println("dkozyaikin@t1-consulting.ru");
    }

    public static void displayInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    private static void exit() {
        System.exit(0);
    }

}
