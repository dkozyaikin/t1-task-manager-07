package ru.t1.dkozyaikin.tm.model;

import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.*;
import static ru.t1.dkozyaikin.tm.constant.TerminalConst.*;

public class Command {

    public static Command VERSION = new Command(
            CMD_VERSION, ARG_VERSION,
            "Display app version."
    );

    public static Command ABOUT = new Command(
            CMD_ABOUT, ARG_ABOUT,
            "Display developer info."
    );

    public static Command HELP = new Command(
            CMD_HELP, ARG_HELP,
            "Display app commands."
    );

    public static Command EXIT = new Command(
            CMD_EXIT, null,
            "Close app."
    );

    public static Command INFO = new Command(
            CMD_INFO, ARG_INFO,
            "Display system info."
    );

    private String name;

    private String argument;

    private String description;

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
